# Популяционная модель CMSY 
> English description available below

Модель CMSY относится к группе моделей для анализа ограниченных, неполных данных. Теоретическое описание здесь дано не будет. 

## 1. Входные данные
В качестве входных данных необходимо использовать величины уловов за срок не менее 5 лет + кол-во лет в ретроспективной диагностике. Входные данные располагаются в файле `/input/data.csv`. Разделитель десятичной части - точка (допускается и запятая).

## 2. Параметризация модели
После подготовки входных данных необходимо параметризировать модель в конфигурационной секции в начале файла `run.R`:

``` 
config.population.resilience <- "Medium" # Allowed resilience level: "Very low", "Low", "Medium", "High"
config.retro.years <- 5 # retrospective analysis time window
config.population.name <- "Sprattus sprattus"
config.population.area <- "Black sea, Russian waters"
config.report.author <- "Kulba S."
```

Особое внимание необходимо уделить параметру популяционной гибкости:
```
config.population.resilience <- "Medium" # Allowed resilience level: "Very low", "Low", "Medium", "High"
```
Параметр данной величины необходимо определить из данных статистических исследований Musick, 1999:


|Гибкость|High|Medium|Low|Very low|
|---|---|---|---|---|
|Продолжительность жизни, лет|1-3|4-10|11-30|>30|
|Возраст полового созревания, лет|<1|2-4|5-10|>10|
|Плодовитость, рекруотов на производителя|>10000|100-1000|10-100|>10|
|Коэф-т поп-го роста, r|>0.5|0.16-0.5|0.05-0.15|<0.05|
|Скорость лин-го роста, k (Берталанфи)|>0.3|0.16-0.3|0.05-0.15|<0.05|

## 3. Установка зависимостей
Все зависимости необходимо установить выполнив скрипт `Install.R` целиком:

```
source("Install.R")
```

## 4. Построение модели
Для построения модели необходимо выполнить файл `Run.R` целиком:

```
source("Run.R")
```

## 5. Результаты
После выполнения скрипта `Run.R` все результаты будут в сгенерированном отчете `Report.html`.

## 6. Источники 

  - Froese R, Demirel N, Coro G, Kleisner KM, Winker H (2017) Estimating fisheries reference points from catch and resilience. Fish and Fisheries 18(3): 506-526.
  - Free CM (2018) datalimited2: More stock assessment methods for data-limited fisheries. R package version 0.1.0. https://github.com/cfree14/datalimited2

## 7. Автор
Mikhail Piatinskii, Azov-Black sea branch of VNIRO 

Email: pyatinskiy_m_m@azniirkh.ru. Telegram: [@zenn1989](https://t.me/zenn1989). Twitter: [@followzenn](https://twitter.com/followzenn)


# CMSY DataLimited model 
CMSY is a production datalimited model. No theoretical meaning will shown there.

## 1. Input data
For input data total landings should be used. 5+ year time seria required. Input data should be placed in `/input/data.csv`. 

## 2. Parametrization
Model parametrization available in config section of `run.R`:

``` 
config.population.resilience <- "Medium" # Allowed resilience level: "Very low", "Low", "Medium", "High"
config.retro.years <- 5 # retrospective analysis time window
config.population.name <- "Sprattus sprattus"
config.population.area <- "Black sea, Russian waters"
config.report.author <- "Kulba S."
```

The focal configuration is population resilience:
```
config.population.resilience <- "Medium" # Allowed resilience level: "Very low", "Low", "Medium", "High"
```
reference to Musick, 1999:


|Resilience|High|Medium|Low|Very low|
|---|---|---|---|---|
|Life span, years|1-3|4-10|11-30|>30|
|Maturation age, years|<1|2-4|5-10|>10|
|Rec per spawner|>10000|100-1000|10-100|>10|
|Population growth rate, r|>0.5|0.16-0.5|0.05-0.15|<0.05|
|Bertalanffy linear growth rate, k|>0.3|0.16-0.3|0.05-0.15|<0.05|

## 3. Dependencies
All dependencies can be installed by `Install.R`:

```
source("Install.R")
```

## 4. Run model
Model can be performed by `Run.R`:

```
source("Run.R")
```

## 5. Results
After `Run.R` successful executed all results output in `Report.html`.

## 6. Credentials

  - Froese R, Demirel N, Coro G, Kleisner KM, Winker H (2017) Estimating fisheries reference points from catch and resilience. Fish and Fisheries 18(3): 506-526.
  - Free CM (2018) datalimited2: More stock assessment methods for data-limited fisheries. R package version 0.1.0. https://github.com/cfree14/datalimited2

## 7. Author
Mikhail Piatinskii, Azov-Black sea branch of VNIRO 

Email: pyatinskiy_m_m@azniirkh.ru. Telegram: [@zenn1989](https://t.me/zenn1989). Twitter: [@followzenn](https://twitter.com/followzenn)